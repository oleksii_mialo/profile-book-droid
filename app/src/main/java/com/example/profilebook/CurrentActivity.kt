package com.example.profilebook

import android.app.Activity

object CurrentActivity
{
    private var _instance : Activity? = null

    fun getActivity() : Activity
    {
        return _instance!!;
    }

    fun setActivity(activity: Activity)
    {
        _instance = activity
    }
}