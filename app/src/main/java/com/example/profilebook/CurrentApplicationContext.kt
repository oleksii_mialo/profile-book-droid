package com.example.profilebook

import android.content.Context

object CurrentApplicationContext
{
    private var _instance : Context? = null

    fun getContext() : Context
    {
        return _instance!!;
    }

    fun setContext(context: Context)
    {
        _instance = context
    }
}