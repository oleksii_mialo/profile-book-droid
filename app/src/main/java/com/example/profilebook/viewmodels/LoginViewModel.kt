package com.example.profilebook.viewmodels

import android.app.AlertDialog
import android.content.DialogInterface
import android.view.View
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.navigation.findNavController
import com.example.profilebook.CurrentActivity
import com.example.profilebook.R
import com.example.profilebook.fragments.LoginFragmentDirections
import com.example.profilebook.services.AuthorizationService
import kotlinx.coroutines.launch

class LoginViewModel : ViewModel() {
    private val _login = MutableLiveData<String>()
    val login: MutableLiveData<String> = _login

    private val _password = MutableLiveData<String>()
    val password: MutableLiveData<String> = _password

    fun onOpenSignUpFragment(view: View){
        view.findNavController().navigate(R.id.action_login_to_signup)
    }

    fun onSignInClick(view: View) {
        viewModelScope.launch {
            val isLoginSuccess = AuthorizationService.logIn(_login.value.toString(), _password.value.toString())

            if (isLoginSuccess){
                view.findNavController().navigate(LoginFragmentDirections.actionLoginToProfileList())
            }
            else
            {
                showAlert("Incorrect login or password")
            }
        }
    }

    private fun showAlert(message : String)
    {
        val alert = AlertDialog.Builder(CurrentActivity.getActivity())
            .setTitle("Oops")
            .setMessage(message)
            .setPositiveButton("Ok", DialogInterface.OnClickListener { dialog, id ->
                dialog.cancel()
            })

        alert.show()
    }
}