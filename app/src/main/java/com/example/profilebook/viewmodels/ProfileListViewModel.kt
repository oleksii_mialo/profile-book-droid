package com.example.profilebook.viewmodels

import android.view.View
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.navigation.findNavController
import com.example.profilebook.fragments.ProfileListFragmentDirections
import com.example.profilebook.models.ProfileModel
import com.example.profilebook.services.AuthorizationService
import com.example.profilebook.services.ProfileService
import kotlinx.coroutines.launch

class ProfileListViewModel : ViewModel() {
    private var _profiles = arrayListOf<ProfileModel>()
    var profiles = MutableLiveData<ArrayList<ProfileModel>>()

    init {
        loadProfiles()
    }

    fun loadProfiles()
    {
        viewModelScope.launch {
            _profiles = ArrayList(ProfileService.getAllProfiles())
            profiles.value = _profiles
        }
    }

    fun addProfile(view: View){
        view.findNavController().navigate(ProfileListFragmentDirections.actionProfileListToAddEditProfile(0, "addProfile"))
    }

    fun deleteProfile(profile: ProfileModel){
        viewModelScope.launch {
            ProfileService.deleteProfile(profile)
        }
    }

    fun logout(){
        AuthorizationService.logout()
    }
}