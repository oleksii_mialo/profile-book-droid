package com.example.profilebook.viewmodels

import android.app.AlertDialog
import android.content.DialogInterface
import android.view.View
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import androidx.navigation.findNavController
import com.example.profilebook.CurrentActivity
import com.example.profilebook.models.UserModel
import com.example.profilebook.services.AuthorizationService
import kotlinx.coroutines.launch

class SignUpViewModel : ViewModel() {
    private var _login = MutableLiveData<String>() // ??? It need ???
    var login: MutableLiveData<String> = _login

    private val _password = MutableLiveData<String>()
    val password: MutableLiveData<String> = _password

    private val _confirmPassword = MutableLiveData<String>()
    val confirmPassword: MutableLiveData<String> = _confirmPassword

    fun onSignUpClick(view: View){
        var isDataValid = isLoginValid()

        if (!isDataValid)
        {
            showAlert("Login invalid")
        }
        else
        {
            isDataValid = isPasswordValid()

            if (!isDataValid)
            {
                showAlert("Password invalid")
            }
            else
            {
                isDataValid = isPasswordsMatches()

                if (!isDataValid)
                {
                    showAlert("Passwords mismatches")
                }
                else
                {
                    createNewUser(view)
                }
            }
        }
    }

    private fun createNewUser(view: View)
    {
        viewModelScope.launch {
            if (!AuthorizationService.hasLogin(_login.value.toString())){
                AuthorizationService.createAccount(UserModel(
                    Login = login.value.toString(),
                    HashPassword = password.value.toString()))
                view.findNavController().popBackStack()
            }
            else
            {
                showAlert("User already exist")
            }
        }
    }

    private fun isLoginValid() : Boolean
    {
        return !login.value.isNullOrBlank() && login.value!!.length > 2;
    }

    private fun  isPasswordValid() : Boolean
    {
        val passwordValue = password.value.toString()
        return  passwordValue.contains(Regex("([0-9])")) && passwordValue.contains(Regex("([A-Z])"))
    }

    private fun isPasswordsMatches() : Boolean
    {
        return  password.value.toString() == confirmPassword.value.toString()
    }

    private fun showAlert(message : String)
    {
        val alert = AlertDialog.Builder(CurrentActivity.getActivity())
            .setTitle("Oops")
            .setMessage(message)
            .setPositiveButton("Ok", DialogInterface.OnClickListener { dialog, id ->
                dialog.cancel()
            })

        alert.show()
    }
}