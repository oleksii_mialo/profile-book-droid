package com.example.profilebook.viewmodels

import android.app.AlertDialog
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.profilebook.CurrentActivity
import com.example.profilebook.CurrentApplicationContext
import com.example.profilebook.MainActivity
import com.example.profilebook.models.ProfileModel
import com.example.profilebook.services.AuthorizationService
import com.example.profilebook.services.ProfileService
import kotlinx.coroutines.isActive
import kotlinx.coroutines.launch

class AddEditProfileViewModel : ViewModel() {
    lateinit var profile: ProfileModel

    private val _image = MutableLiveData<String>()
    val image: MutableLiveData<String> = _image

    private val _nickname = MutableLiveData<String>()
    val nickname: MutableLiveData<String> = _nickname

    private val _name = MutableLiveData<String>()
    val name: MutableLiveData<String> = _name

    private val _description = MutableLiveData<String>()
    val description: MutableLiveData<String> = _description

    fun initializeProfileById(id: Int){
        viewModelScope.launch {
            profile = ProfileService.getProfileById(id)
            image.value = profile.ImagePath!!
            nickname.value = profile.Nickname
            name.value = profile.Name
            description.value = profile.Description
        }
    }

    fun onProfileSaved() : Boolean{
        var result = false

        if (!hasEmptyFields()){
            if (!this::profile.isInitialized){
                profile = createProfile()
            }
            else{
                editProfile()
            }

            viewModelScope.launch {
                if(isActive)
                 ProfileService.saveProfile(profile)
            }

            result = true
        }
        else{
            showErrorAlert("Name, Nickname and description is requirement fields")
        }

        return result
    }

    private fun hasEmptyFields() : Boolean{
        return nickname.value.isNullOrBlank() || name.value.isNullOrBlank()
    }

    private fun createProfile() : ProfileModel{
        return ProfileModel(
            ImagePath = image.value,
            Nickname = nickname.value.toString(),
            Name = name.value.toString(),
            Description = description.value.toString(),
            UserId = AuthorizationService.currentUserId
        )
    }

    private fun editProfile() {
        profile.ImagePath = image.value
        profile.Nickname = nickname.value.toString()
        profile.Name = name.value.toString()
        profile.Description = description.value.toString()
    }

    private fun showErrorAlert(message: String){
        val builder = AlertDialog.Builder(CurrentActivity.getActivity())
        builder.setTitle("Alert")
            .setMessage(message)
            .setPositiveButton("Ok") { dialog, which ->
                dialog.cancel()
            }.create()

        builder.show()
    }
}