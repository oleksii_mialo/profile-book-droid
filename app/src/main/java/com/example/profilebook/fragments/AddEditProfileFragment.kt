package com.example.profilebook.fragments

import android.app.Activity.RESULT_OK
import android.app.AlertDialog
import android.content.DialogInterface
import android.content.Intent
import android.graphics.Bitmap
import android.net.Uri
import android.os.Bundle
import android.provider.MediaStore
import android.view.*
import androidx.fragment.app.Fragment
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.profilebook.R
import com.example.profilebook.databinding.AddEditProfileFragmentBinding
import com.example.profilebook.services.ImageSaverService
import com.example.profilebook.viewmodels.AddEditProfileViewModel

class AddEditProfileFragment : Fragment() {
    private lateinit var binding: AddEditProfileFragmentBinding
    private lateinit var viewModel: AddEditProfileViewModel
    private val args by navArgs<AddEditProfileFragmentArgs>()

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.add_edit_profile_fragment,
            container,
            false)

        viewModel = ViewModelProvider(this)[AddEditProfileViewModel::class.java]
        binding.viewModel = viewModel
        binding.lifecycleOwner = viewLifecycleOwner

        binding.profileImage.setOnClickListener { onAddProfileImage() }

        if (args.profileId != 0){
            viewModel.initializeProfileById(args.profileId)
        }

        viewModel.image.observe(viewLifecycleOwner, Observer {
            if (it != null){
                val image = Uri.parse(it)
                binding.profileImage.setImageURI(image)
            }
        })

        setHasOptionsMenu(true)

        return binding.root
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.save_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId){
            R.id.save_action -> {
                if (viewModel.onProfileSaved()){
                    findNavController().popBackStack()
                }
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }

    private fun onAddProfileImage() {
        val builder = AlertDialog.Builder(activity)
        builder.setTitle("getString(R.string.AddPhoto)")
        builder.setItems(arrayOf(
            "getString(R.string.Gallery)",
            "getString(R.string.Camera)" ),
            DialogInterface.OnClickListener { dialog, which ->
                chooseAction(dialog, which)
            })

        builder.create().show()
    }

    private fun chooseAction(dialog: DialogInterface?, which: Int) {
        when (which){
            0 -> {
                val galleryIntent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI)
                galleryIntent.type = "image/"
                startActivityForResult(galleryIntent, 0)
            }
            else -> {
                val cameraIntent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                startActivityForResult(cameraIntent, 1)
            }
        }
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        when (requestCode){
            0 -> {
                if (resultCode == RESULT_OK) {
                    val imagePath = ImageSaverService.saveImageFromUri(this.context, data?.data!!)
                    viewModel.image.value = imagePath
                }
            }
            1 -> {
                if (resultCode == RESULT_OK){
                    val imageBitmap = data?.extras?.get("data") as Bitmap
                    binding.profileImage.setImageBitmap(imageBitmap)
                    val imagePath = ImageSaverService.saveImageFromBitmap(this.context, imageBitmap)
                    viewModel.image.value = imagePath
                }
            }
        }
    }
}