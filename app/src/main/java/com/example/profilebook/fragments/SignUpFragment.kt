package com.example.profilebook.fragments

import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import com.example.profilebook.R
import com.example.profilebook.databinding.LoginFragmentBinding
import com.example.profilebook.databinding.SignUpFragmentBinding
import com.example.profilebook.viewmodels.LoginViewModel
import com.example.profilebook.viewmodels.SignUpViewModel

class SignUpFragment : Fragment() {
    private lateinit var binding: SignUpFragmentBinding
    private lateinit var viewModel: SignUpViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.sign_up_fragment,
            container,
            false)
        viewModel = ViewModelProvider(this)[SignUpViewModel::class.java]
        binding.context = viewModel
        binding.lifecycleOwner = viewLifecycleOwner

        return binding.root
    }
}