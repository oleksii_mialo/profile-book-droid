package com.example.profilebook.fragments

import android.os.Bundle
import android.view.*
import androidx.fragment.app.Fragment
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.Observer
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import com.example.profilebook.R
import com.example.profilebook.models.ProfileModel
import com.example.profilebook.viewmodels.ProfileListViewModel
import com.example.profilebook.databinding.ProfileListFragmentBinding
import com.example.profilebook.list_adapters.ProfileActionListener
import com.example.profilebook.list_adapters.ProfilesAdapter

class ProfileListFragment : Fragment() {
    private lateinit var binding: ProfileListFragmentBinding
    private lateinit var viewModel: ProfileListViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.profile_list_fragment,
            container,
            false)
        viewModel = ViewModelProvider(this)[ProfileListViewModel::class.java]
        binding.viewModel = viewModel
        binding.lifecycleOwner = viewLifecycleOwner

        val layoutManager = LinearLayoutManager(activity)
        binding.recyclerView.layoutManager = layoutManager

        val profileActionListener = object : ProfileActionListener{
            override fun onEditProfile(profile: ProfileModel) {
                findNavController().navigate(ProfileListFragmentDirections.actionProfileListToAddEditProfile(profile.Id, "Edit profile"))
            }

            override fun onDeleteProfile(profile: ProfileModel) {
                viewModel.deleteProfile(profile)
            }

            override fun onProfileTapped(profile: ProfileModel) {
                findNavController().navigate(ProfileListFragmentDirections.actionProfileIcon(profile.ImagePath ?: ""))
            }
        }

        viewModel.profiles.observe(viewLifecycleOwner, Observer {
            val adapter = ProfilesAdapter(it, profileActionListener)
            binding.recyclerView.adapter = adapter
        })

        setHasOptionsMenu(true)

        return binding.root
    }

    override fun onStart() {
        super.onStart()

        viewModel.loadProfiles()
    }

    override fun onCreateOptionsMenu(menu: Menu, inflater: MenuInflater) {
        super.onCreateOptionsMenu(menu, inflater)
        inflater.inflate(R.menu.profiles_menu, menu)
    }

    override fun onOptionsItemSelected(item: MenuItem): Boolean {
        return when (item.itemId){
            R.id.logout_action -> {
                viewModel.logout()
                findNavController().navigate(ProfileListFragmentDirections.actionProfileListLogout())
                true
            }
            else -> super.onOptionsItemSelected(item)
        }
    }
}