package com.example.profilebook.fragments

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import com.example.profilebook.viewmodels.LoginViewModel
import com.example.profilebook.R
import com.example.profilebook.databinding.LoginFragmentBinding

class LoginFragment : Fragment() {
    private lateinit var binding: LoginFragmentBinding
    private lateinit var viewModel: LoginViewModel

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.login_fragment,
            container,
            false)
        viewModel = ViewModelProvider(this)[LoginViewModel::class.java]
        binding.context = viewModel
        binding.lifecycleOwner = viewLifecycleOwner

        return binding.root
    }
}