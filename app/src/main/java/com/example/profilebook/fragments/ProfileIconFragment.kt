package com.example.profilebook.fragments

import android.net.Uri
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.navigation.fragment.navArgs
import com.example.profilebook.R
import com.example.profilebook.databinding.LoginFragmentBinding
import com.example.profilebook.databinding.ProfileIconFragmentBinding
import com.example.profilebook.viewmodels.LoginViewModel
import com.example.profilebook.viewmodels.ProfileIconViewModel

class ProfileIconFragment : Fragment(),View.OnClickListener {
    private lateinit var binding: ProfileIconFragmentBinding
    private lateinit var viewModel: ProfileIconViewModel
    private val args by navArgs<ProfileIconFragmentArgs>()

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        binding = DataBindingUtil.inflate(
            inflater,
            R.layout.profile_icon_fragment,
            container,
            false)
        viewModel = ViewModelProvider(this)[ProfileIconViewModel::class.java]
        binding.context = viewModel
        binding.lifecycleOwner = viewLifecycleOwner

        if(args.imagePath != "")
        {
            val image = Uri.parse(args.imagePath)
            binding.profileImage.setImageURI(image)
        }

        binding.root.setOnClickListener(this)

        return binding.root
    }

    override fun onClick(view: View?) {
        findNavController().popBackStack()
    }
}