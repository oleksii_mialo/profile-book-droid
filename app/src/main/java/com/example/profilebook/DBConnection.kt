package com.example.profilebook

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import com.example.profilebook.models.IProfileAccessor
import com.example.profilebook.models.ProfileModel
import com.example.profilebook.models.UserAccessor
import com.example.profilebook.models.UserModel

@Database(
    entities = [
        UserModel::class,
        ProfileModel::class],
    version = 1,
    exportSchema = false)
abstract class DBConnection : RoomDatabase() {
    abstract val userAccessor : UserAccessor
    abstract val profileAccessor : IProfileAccessor

    companion object{
        @Volatile
        private var _instance: DBConnection? = null

        fun getInstance(context: Context): DBConnection {
            synchronized(this){
                var instance = _instance

                if (instance == null){
                    instance = Room.databaseBuilder(
                        context.applicationContext,
                        DBConnection::class.java,
                        "db.sqlite"
                    )
                        .fallbackToDestructiveMigration()
                        .build()
                    _instance = instance
                }

                return instance
            }
        }
    }
}