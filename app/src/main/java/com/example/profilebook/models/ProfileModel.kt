package com.example.profilebook.models

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "profile")
data class ProfileModel (
    @PrimaryKey(autoGenerate = true)
    var Id: Int = 0,

    var Name: String,

    var Nickname: String,

    var Description: String,

    var ImagePath: String?,

    var UserId: Int
)