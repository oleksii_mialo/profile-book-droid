package com.example.profilebook.models

import androidx.room.Entity
import androidx.room.PrimaryKey

@Entity(tableName = "user")
data class UserModel (
    @PrimaryKey(autoGenerate = true)
    val Id: Int = 0,

    val Login: String,

    val HashPassword: String
)