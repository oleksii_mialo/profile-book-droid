package com.example.profilebook.models

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query

@Dao
interface UserAccessor {
    @Insert
    suspend fun insert(user: UserModel)

    @Query("SELECT * from user WHERE Login = :login AND HashPassword = :password")
    suspend fun get(login: String, password: String): UserModel?

    @Query("SELECT * from user WHERE Login = :login")
    suspend fun exist(login: String): UserModel?
}