package com.example.profilebook.models

import androidx.room.*

@Dao
interface IProfileAccessor {
    @Insert
    suspend fun insert(profile: ProfileModel)

    @Update
    suspend fun update(profile: ProfileModel)

    @Delete
    suspend fun delete(profile: ProfileModel)

    @Query("SELECT * from profile WHERE UserId = :UserId")
    suspend fun getAll(UserId: Int) : List<ProfileModel>

    @Query("SELECT * from profile WHERE id = :id")
    suspend fun getProfileById(id: Int) : ProfileModel
}