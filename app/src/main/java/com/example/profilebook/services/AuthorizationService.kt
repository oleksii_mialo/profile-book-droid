package com.example.profilebook.services

import android.content.Context
import com.example.profilebook.CurrentApplicationContext
import com.example.profilebook.DBConnection
import com.example.profilebook.models.UserModel

object AuthorizationService {
    private var _connection: DBConnection = DBConnection.getInstance(CurrentApplicationContext.getContext())
    private val _preferences = CurrentApplicationContext.getContext().getSharedPreferences("com.headworks.profilebook.preferences", Context.MODE_PRIVATE)

    val isAuthorized = _preferences.getInt("userId", 0) != 0

    val currentUserId : Int
        get() = _preferences.getInt("userId", 0)

    suspend fun logIn(login: String, password: String): Boolean {
        val user = _connection.userAccessor.get(login, password)

        if (user != null){
            _preferences.edit()
                .putInt("userId", user.Id)
                .apply()
        }

        return user != null
    }

    suspend fun createAccount(user: UserModel) {
        _connection.userAccessor.insert(user)
    }

    suspend fun hasLogin(login: String) : Boolean{
        return _connection.userAccessor.exist(login) != null
    }

    fun logout(){
        _preferences.edit()
            .putInt("userId", 0)
            .apply()
    }
}