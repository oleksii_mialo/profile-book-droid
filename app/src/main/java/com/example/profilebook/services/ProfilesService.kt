package com.example.profilebook.services

import android.util.Log
import com.example.profilebook.CurrentApplicationContext
import com.example.profilebook.DBConnection
import com.example.profilebook.models.ProfileModel

object ProfileService {
    private var _database: DBConnection = DBConnection.getInstance(CurrentApplicationContext.getContext())

    suspend fun getAllProfiles() : List<ProfileModel>{
        val userId = AuthorizationService.currentUserId
        return _database.profileAccessor.getAll(userId)
    }

    suspend fun getProfileById(id: Int) : ProfileModel{
        return _database.profileAccessor.getProfileById(id)
    }

    suspend fun saveProfile(profile: ProfileModel) {
            if (profile.Id == 0) {
                _database.profileAccessor.insert(profile)
            } else {
                _database.profileAccessor.update(profile)
            }
    }

    suspend fun deleteProfile(profile: ProfileModel){
        _database.profileAccessor.delete(profile)
    }
}