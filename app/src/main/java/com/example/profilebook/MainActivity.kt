package com.example.profilebook

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import androidx.appcompat.app.AlertDialog
import androidx.navigation.NavController
import androidx.navigation.NavDestination
import androidx.navigation.findNavController
import com.example.profilebook.services.AuthorizationService
import java.util.regex.Pattern

class MainActivity : AppCompatActivity() {
    private var _navController: NavController? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        val navController = findNavController(R.id.navigationHost)

        CurrentActivity.setActivity(this)
        CurrentApplicationContext.setContext(this)

        prepareRootNavController(navController)

        onNavControllerActivated(navController)
    }

    override fun onSupportNavigateUp(): Boolean {
        return _navController?.navigateUp() ?: false || super.onSupportNavigateUp()
    }

    private fun onNavControllerActivated(navController: NavController) {
        if (_navController != navController){
            _navController?.removeOnDestinationChangedListener(destinationListener)
            navController.addOnDestinationChangedListener(destinationListener)
            _navController = navController
        }
    }

    private fun prepareRootNavController(navController: NavController) {
        val graph = navController.navInflater.inflate(R.navigation.main_navigation)

        val fragmentId = if (AuthorizationService.isAuthorized) {
            R.id.profileListFragment
        } else {
            R.id.logInFragment
        }

        graph.startDestination = fragmentId
        navController.graph = graph
    }

    private val destinationListener = NavController.OnDestinationChangedListener { _, destination, arguments ->
        supportActionBar?.title = destination.label
        supportActionBar?.setDisplayHomeAsUpEnabled(!isStartDestination(destination))
    }

    private fun isStartDestination(destination: NavDestination?): Boolean {
        var result = false

        if (destination != null) {
            val graph = destination.parent
            if (graph != null){
                val startDestinations = setOf(R.id.profileListFragment, R.id.logInFragment) + graph.startDestination
                result = startDestinations.contains(destination.id)
            }
        }

        return result
    }

}