package com.example.profilebook.list_adapters

import android.net.Uri
import android.view.*
import androidx.recyclerview.widget.RecyclerView
import com.example.profilebook.CurrentActivity
import com.example.profilebook.R
import com.example.profilebook.databinding.ProfileCellBinding
import com.example.profilebook.models.ProfileModel

class ProfilesAdapter(private val profiles: ArrayList<ProfileModel>,
                      private val actionListener: ProfileActionListener)
    : RecyclerView.Adapter<ProfilesAdapter.ProfilesViewHolder>(), View.OnLongClickListener, View.OnClickListener {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ProfilesViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val binding = ProfileCellBinding.inflate(inflater, parent, false)
        binding.root.setOnLongClickListener(this)
        binding.root.setOnClickListener(this)
        return ProfilesViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ProfilesViewHolder, position: Int) {
        val profile = profiles[position]
        holder.itemView.tag = profile
        holder.bind(profile)
    }

    override fun getItemCount(): Int {
        return profiles.size
    }

    override fun onClick(view: View?) {
        val profile = view?.tag as ProfileModel
        actionListener.onProfileTapped(profile)
    }

    private var selectedProfile : ProfileModel? = null
    override fun onLongClick(view: View?): Boolean {
        selectedProfile = view?.tag as ProfileModel
        showContextAction()
        return true
    }

    private val actionModeCallback = object : ActionMode.Callback {
        override fun onCreateActionMode(mode: ActionMode, menu: Menu): Boolean {
            val inflater: MenuInflater = mode.menuInflater
            inflater.inflate(R.menu.profile_detail_menu, menu)
            return true
        }

        override fun onPrepareActionMode(mode: ActionMode, menu: Menu): Boolean {
            return false
        }

        override fun onActionItemClicked(mode: ActionMode, item: MenuItem): Boolean {

            return when (item.itemId) {
                R.id.delete_option -> {
                    profiles.remove(selectedProfile)
                    actionListener.onDeleteProfile(selectedProfile!!)
                    mode.finish()
                    true
                }
                R.id.edit_option -> {
                    actionListener.onEditProfile(selectedProfile!!)
                    mode.finish()
                    true
                }
                else -> false
            }
        }

        override fun onDestroyActionMode(mode: ActionMode) {
        }
    }

    fun showContextAction()
    {
        CurrentActivity.getActivity().startActionMode(actionModeCallback)
    }

    class ProfilesViewHolder(
        val binding: ProfileCellBinding
    ) : RecyclerView.ViewHolder(binding.root) {

        fun bind(profile: ProfileModel) = with(binding) {
            if (profile.ImagePath != null) {
                val image = Uri.parse(profile.ImagePath)
                profileImage.setImageURI(image)
            }
            profileNickname.text = profile.Nickname
            profileName.text = profile.Name
        }
    }
}