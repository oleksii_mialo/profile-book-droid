package com.example.profilebook.list_adapters

import com.example.profilebook.models.ProfileModel

interface ProfileActionListener{
    fun onEditProfile(profile: ProfileModel)

    fun onDeleteProfile(profile: ProfileModel)

    fun onProfileTapped(profile: ProfileModel)
}